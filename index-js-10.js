document.addEventListener("DOMContentLoaded", () => {
  const listTabs = document.querySelectorAll(".tabs li");
  const listContent = document.querySelectorAll(".tabs-content li");

  listContent.forEach((item) => {
    item.hidden = true;
  });

  listTabs.forEach((li, index) => {
    li.setAttribute("data-value", index + 1);
    const valueTabs = +(li.getAttribute("data-value"));

    li.addEventListener("click", () => {
      listTabs.forEach((active) => {
        active.classList.remove("active");
      });
      li.classList.add("active");

      listContent.forEach((item, index) => {
        item.hidden = true;
        if (index + 1 === valueTabs) {
          item.hidden = false;
        }
      });
    });
  });
});
